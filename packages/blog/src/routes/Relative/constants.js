export const RELATIVE = 'relative';

export const RELATIVE_ARTICLE = 'RELATIVE_ARTICLE';
export const ARTICLE_TAG = 'ARTICLE_TAG';
export const CLEAR_RELATIVE = 'CLEAR_RELATIVE';
